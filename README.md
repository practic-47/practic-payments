# practic-payments

Payments facade for abstraction over real payments providers, prevents of fraud and other `bad` activities.


## Build

```
mvn clean install
```

## Depoly

```
docker build -t humb1t/practic-payments:1.0 .
```

## Start

```
docker run -p 127.0.0.1:7000:7000 humb1t/practic-payments:1.0
```
