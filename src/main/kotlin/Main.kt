import io.javalin.Javalin
import io.javalin.apibuilder.ApiBuilder.*
import io.micrometer.core.instrument.Metrics
import io.micrometer.prometheus.PrometheusConfig
import io.micrometer.prometheus.PrometheusMeterRegistry
import java.time.Instant
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import io.javalin.json.JavalinJson
import io.javalin.json.FromJsonMapper
import io.javalin.json.ToJsonMapper
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.JsonClass
import com.squareup.moshi.Moshi

fun main(args: Array<String>) {
    JavalinApp(7000, HashSet()).init()
}

class JavalinApp(
        private val port: Int,
        private val payments: MutableCollection<Payment>
) {
    private val pageSize: Int = 10
    private val registry = PrometheusMeterRegistry(PrometheusConfig.DEFAULT)

    fun init(): Javalin {
	val moshi = Moshi.Builder().build()
	val gson = GsonBuilder().create()
	JavalinJson.fromJsonMapper = object : FromJsonMapper {
		override fun <T> map(json: String, targetClass: Class<T>) = 
			moshi.adapter(targetClass).fromJson(json)!!
	}
	JavalinJson.toJsonMapper = object : ToJsonMapper {
		override fun map(obj: Any): String = gson.toJson(obj)
	}
        val app = Javalin.create().apply {
            port(port)
            exception(Exception::class.java) { e, _ -> e.printStackTrace() }
	    disableStartupBanner()
	    enableMicrometer()
        }.start()
	Metrics.globalRegistry.add(registry)
	app.routes {
		path("api") {
			path("payments") {
				get { ctx -> ctx.json(payments.take(pageSize)) }
				put {
					ctx -> 
						payments.add(
								ctx.body<PaymentRequest>().toPayment()
						)
						ctx.status(200)
				}
			}
		}
		path("metrics") {
			get("/prometheus") { ctx -> ctx.result(registry.scrape()) }
		}
	}
        return app
    }
}

class Payment(
		val id: Int,
		val creationDate: Instant = Instant.now(),
		val sender: Int,
		val receiver: Int,
		val value: Int,
		val currency: String,
		val status: Status,
		val additionalInformation: String
	) {
		enum class Status {
			Pending
		}
	}

@JsonClass(generateAdapter = true)
data class PaymentRequest(
		val receiver: Int,
		val value: Int,
		val currency: String,
		val additionalInformation: String = ""
) {
	fun toPayment(): Payment {
		return Payment(
				id = Instant.now().getNano(),
				sender = 1,
				receiver = this.receiver,
				value = this.value,
				currency = this.currency,
				status = Payment.Status.Pending,
				additionalInformation = this.additionalInformation
		)
	}
}
