import cucumber.api.PendingException
import cucumber.api.java8.En
import io.javalin.json.JavalinJson
import org.junit.Assert

class SystemStepDefs(val testInstance: TestInstance) : En {
	init {
		Given("^system is started on port (\\d+)$") { port: Int -> 
			testInstance.app = JavalinApp(port, testInstance.payments).init()
		}
	}
}

class PaymentStepDefs(val testInstance: TestInstance) : En {
        private val url = "http://localhost:"

  init {

	  Given("^payment is (\\d+) \"([^\"]*)\" to receiver with id (\\d+)$") {
		  value: Int, currency: String, receiver: Int ->
			  testInstance.paymentRequest = PaymentRequest(
					  value = value,
					  currency = currency,
					  receiver = receiver
			  )
	}

	When("^client sends payment$") {
		testInstance.result = khttp.put(
				url+testInstance.app.port()+"/api/payments",
				data = JavalinJson.toJson(
						testInstance.paymentRequest
				)

		)
	}

	Then("^response is OK$") {
		Assert.assertEquals(200, testInstance.result.statusCode)
	}


	Given("^there are (\\d+) payments$") { count: Int ->
		for (i in 1..count) {
			testInstance.payments.add(createTestPayment(i))
		}
	}

	When("^request to list all payments sent$") {
		testInstance.result = khttp.get(url = this.url + testInstance.app.port() + "/api/payments")
	}

	Then("^response contains (\\d+) elements$") { totalCount: Int -> 
		Assert.assertEquals(totalCount, testInstance.result.jsonArray.length())
	}

	Then("^the first payment contains `id`, `creationDate`, `sender`, `receiver`, `value`, `currency`, `status`, `additionalInformation` fields$") {
		val firstPayment = testInstance.result.jsonArray.getJSONObject(0)
		Assert.assertNotNull(firstPayment.get("id")!!)
		Assert.assertNotNull(firstPayment.get("creationDate")!!)
		Assert.assertNotNull(firstPayment.get("sender")!!)
		Assert.assertNotNull(firstPayment.get("receiver")!!)
		Assert.assertNotNull(firstPayment.get("value")!!)
		Assert.assertNotNull(firstPayment.get("currency")!!)
		Assert.assertNotNull(firstPayment.get("status")!!)
		Assert.assertNotNull(firstPayment.get("additionalInformation")!!)
	}
  }

  fun createTestPayment(id: Int): Payment {
  	return Payment(
			id = id,
			sender = id*100,
			receiver = id*150,
			value = id*10,
			currency = "Rub",
			status = Payment.Status.Pending,
			additionalInformation = "_"
		)
  }
}
