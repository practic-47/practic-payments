import io.javalin.Javalin
import khttp.responses.Response

/**
 * `World` which contains information shared in scope of test scenario.
 *
 * @author Nikita_Puzankov
 */
class TestInstance {
    lateinit var app: Javalin
    lateinit var result: Response
    lateinit var paymentRequest: PaymentRequest
    val payments: MutableCollection<Payment> = HashSet()
}
