Feature: Send Payment
	As a client, I want to send valid payments to another clients.

	Scenario: Client sends valid payment.
		Given system is started on port 7007
		And payment is 100 "RUB" to receiver with id 10001
		When client sends payment
		Then response is OK
