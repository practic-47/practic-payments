Feature: Look for payments 

	The practic payments system provides a list of all payments in a common representation which 
	hides differences between real payments providers. Administrator can look for a payments by request.

	Scenario: Element contains all information
		Given system is started on port 7000
		And there are 1 payments
		When request to list all payments sent
		Then response contains 1 elements
		And the first payment contains `id`, `creationDate`, `sender`, `receiver`, `value`, `currency`, `status`, `additionalInformation` fields
		
	Scenario Outline: Paginate response
		Given system is started on port <port>
		And there are <count> payments
		When request to list all payments sent
		Then response contains <size> elements

	Examples:
		| port | count | size |
		| 7001 | 20    | 10   |
		| 7002 | 10    | 10   |
		| 7003 | 5     | 5    |
		| 7004 | 0     | 0    |
