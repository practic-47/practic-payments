FROM birdy/graalvm:latest

ADD target/practic-payments-1.0-SNAPSHOT-jar-with-dependencies.jar /tmp/app.jar
ADD reflection.json /tmp/reflection.json
RUN cd /tmp && native-image -jar app.jar -H:ReflectionConfigurationFiles=reflection.json -H:+JNI \
  -H:Name=practic-payments --static --delay-class-initialization-to-runtime=io.javalin.json.JavalinJson

FROM scratch
COPY --from=0 /tmp/practic-payments /
ENTRYPOINT ["/practic-payments"]
